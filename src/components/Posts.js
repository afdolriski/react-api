import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions/postAction'

class Posts extends Component {
    componentWillMount() {
        this.props.fetchPosts();
    }

    componentWillReceiveProps(newProps) {
        if (newProps.post) {
            this.props.posts.unshift(newProps.post);
        }
    }

    render() {
        const posts = this.props.posts.map(post => {
            return <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
            </div>
        });
        return (
            <div>
                <h1>Posts</h1>
                {posts}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts,
    post: state.posts.post
});

export default connect(mapStateToProps, { fetchPosts })(Posts);